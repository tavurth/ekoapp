import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { stepReset } from 'containers/HomePage/actions';
import { withConnect } from './mapStateToProps';

/**
 * Shows the button to return to the previous step if we're not on the initial.
 * @param {number} currentStep - Current step of the home page process.
 * @returns {React.Component}
 */
function ResetButton({ currentStep }) {
    if (currentStep === 0) {
        return null;
    }

    return (
        <Link className="router-link" to="/" onClick={stepReset}>
            Reset
        </Link>
    );
}

ResetButton.propTypes = {
    currentStep: PropTypes.number.isRequired,
};

export default withConnect(ResetButton);
