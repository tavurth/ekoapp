import React from 'react';

import './style.scss';
import Banner from './images/banner.jpg';
import ResetStepButton from './ResetButton';

function Header() {
    return (
        <div className="header">
            <div className="nav-bar">
                <ResetStepButton />
            </div>
            <a href="https://github.com/tavurth">
                <img src={Banner} alt="react-redux-boilerplate - Logo" />
            </a>
        </div>
    );
}

export default Header;
