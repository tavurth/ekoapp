import { connect } from 'react-redux';

import { createStructuredSelector } from 'reselect';
import { makeSelectStep } from 'containers/HomePage/selectors';

export const mapStateToProps = createStructuredSelector({
    currentStep: makeSelectStep(),
});

export const withConnect = connect(mapStateToProps);
