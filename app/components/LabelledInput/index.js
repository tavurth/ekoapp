import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';
import Input from '../Input';

/**
 * Returns a styled input field.
 * Adds a label above the input area
 *
 * @param {function} setRef - Reference setter for the parent component.
 * @param {string} type - text or checkbox.
 * @param {string} label - Label to display above the input area.
 * @param {function} onChange - Called on each change of input.
 * @param {string|boolean} defaultValue - Default value for the input area if any.
 * @returns {React.Component}
 */
function StyledInput({ label, setRef, type, onChange, defaultValue }) {
    return (
        <div className="styled-input">
            <span>{label}</span>
            <Input type={type} setRef={setRef} onChange={onChange} defaultValue={defaultValue} />
        </div>
    );
}

StyledInput.propTypes = {
    setRef: PropTypes.func,
    type: PropTypes.string,
    label: PropTypes.string,
    defaultValue: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
};

StyledInput.defaultProps = {
    label: '',
    type: 'text',
    setRef: () => {},
    defaultValue: '',
};

export default StyledInput;
