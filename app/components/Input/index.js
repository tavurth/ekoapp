import React from 'react';
import PropTypes from 'prop-types';

/**
 * Returns a basic styled input field.
 * Can be either a checkbox or a text input
 *
 * @param {function} setRef - Reference setter for the parent component.
 * @param {string} type - text or checkbox.
 * @param {function} onChange - Called on each change of input.
 * @param {string|boolean} defaultValue - Default value for the input area if any.
 * @returns {React.Component}
 */
function Input({ setRef, type, onChange, defaultValue }) {
    if (onChange instanceof Function) {
        return <input type={type} ref={setRef} defaultValue={defaultValue} onChange={onChange} />;
    }

    return <input type={type} ref={setRef} defaultValue={defaultValue} />;
}

Input.propTypes = {
    setRef: PropTypes.func,
    type: PropTypes.string,
    defaultValue: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
};

Input.defaultProps = {
    type: 'text',
    setRef: () => {},
    defaultValue: '',
};

export default Input;
