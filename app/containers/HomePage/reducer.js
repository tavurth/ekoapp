/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';

import { STEP_NEXT, STEP_RESET } from './constants';

// The initial state of the App
const initialState = fromJS({
    step: 0,
    loading: false,
});

function appReducer(state = initialState, action) {
    switch (action.type) {
        case STEP_NEXT:
            return state.set('step', state.get('step') + 1);
        case STEP_RESET:
            return state.set('step', 0);
        default:
            return state;
    }
}

export default appReducer;
