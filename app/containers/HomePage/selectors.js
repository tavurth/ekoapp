/**
 * The global state selectors
 */

import { createSelector } from 'reselect';

const selectGlobal = (state) => state.get('home');

const makeSelectLoading = () => createSelector(selectGlobal, (globalState) => globalState.get('loading'));

const makeSelectStep = () => createSelector(selectGlobal, (globalState) => globalState.get('step'));

export { selectGlobal, makeSelectLoading, makeSelectStep };
