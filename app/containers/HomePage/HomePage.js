/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';

import DataDisplay from 'containers/DataDisplay';
import TownDataInput from 'containers/DataInput';
import './style.scss';

function HomePage({ currentStep }) {
    switch (currentStep) {
        case 0:
            return <TownDataInput />;
        case 1:
            return <DataDisplay />;
        default:
            return null;
    }
}

HomePage.propTypes = {
    currentStep: PropTypes.number.isRequired,
};

export default HomePage;
