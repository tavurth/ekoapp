import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectLoading, makeSelectStep } from './selectors';
import HomePage from './HomePage';

const mapStateToProps = createStructuredSelector({
    currentStep: makeSelectStep(),
    loading: makeSelectLoading(),
});

const withConnect = connect(mapStateToProps);

export default compose(withConnect)(HomePage);
