/**
 * The global state selectors
 */

import { createSelector } from 'reselect';

const selectData = (state) => state.get('data');

const makeSelectRawData = () => createSelector(selectData, (dataState) => dataState.get('rawData'));

const makeSelectProcessedData = () => createSelector(selectData, (dataState) => dataState.get('processedData'));

export { selectData, makeSelectRawData, makeSelectProcessedData };
