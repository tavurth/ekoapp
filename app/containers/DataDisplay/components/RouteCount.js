import React from 'react';
import PropTypes from 'prop-types';

import styles from './style.scss';

/**
 * Returns a simple string containing the number of routes found.
 * Includes specifics for 0 routes found and perhaps others.
 *
 * @param {number} nRoutes - Number of routes found for the current search.
 * @returns {string|number}
 */
function getRouteCountText(nRoutes) {
    if (nRoutes === 0) {
        return 'No routes found';
    }

    return nRoutes;
}

/**
 * Returns a formatted result container.
 *
 * @param {number} children - Number of routes found.
 * @param {string} label - Optional header for the title.
 * @returns {React.Component}
 */
function RouteCount({ label, children }) {
    return (
        <div className="column">
            <span>{label}</span>
            <span>{children}</span>
        </div>
    );
}

RouteCount.propTypes = {
    label: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
};

RouteCount.defaultProps = {
    label: '',
};

export default RouteCount;
