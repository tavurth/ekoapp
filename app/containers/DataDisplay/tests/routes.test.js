/**
 * Number of routes data processing
 */
import { processData } from 'containers/DataInput/actions';
import { arrayToEdgeMap, calculateRoutes } from '../routeExplorer';

const processedDataConfigurations = [
    {
        name: 'transforms data correctly',
        data: [{ townA: 'A', townB: 'B', distance: 1, edge: 'AB' }],
        output: new Map([['A', new Map([['B', 1]])]]),
    },
    {
        name: 'handles duplicate values',
        data: [
            { townA: 'A', townB: 'B', distance: 1, edge: 'AB' },
            { townA: 'A', townB: 'B', distance: 1, edge: 'AB' },
        ],
        output: new Map([['A', new Map([['B', 1]])]]),
    },
    {
        name: 'transforms larger data sets correctly',
        data: [
            { townA: 'A', townB: 'B', distance: 1, edge: 'AB' },
            { townA: 'A', townB: 'C', distance: 1, edge: 'AC' },
            { townA: 'A', townB: 'D', distance: 1, edge: 'AD' },
            { townA: 'A', townB: 'F', distance: 1, edge: 'AF' },
            { townA: 'A', townB: 'G', distance: 1, edge: 'AG' },
            { townA: 'B', townB: 'A', distance: 1, edge: 'BA' },
            { townA: 'B', townB: 'C', distance: 1, edge: 'BC' },
            { townA: 'B', townB: 'D', distance: 1, edge: 'BD' },
        ],
        output: new Map([
            ['A', new Map([['B', 1], ['C', 1], ['D', 1], ['F', 1], ['G', 1]])],
            ['B', new Map([['A', 1], ['C', 1], ['D', 1]])],
        ]),
    },
];

describe('correctly transforms processed data to Map of Maps', () => {
    describe('transforms data correctly', () => {
        processedDataConfigurations.forEach(({ options, data, output, name }) => {
            it(name, () => {
                expect(arrayToEdgeMap(data)).toEqual(output);
            });
        });
    });
});

const routeConfigurations = [
    // One route configuration
    {
        name: 'finds a single edge',
        output: 1,
        edges: 'AB1',
        options: { posA: 'A', posB: 'B', maxStops: 1 },
    },
    {
        name: 'does not find a single edge when none exists',
        output: 0,
        edges: 'AB1',
        options: { posA: 'B', posB: 'C', maxStops: 50 },
    },

    // Two routes configuration
    {
        name: 'does not find a single edge with no stops available',
        output: 0,
        edges: 'AB1, BC2',
        options: { posA: 'A', posB: 'C', maxStops: 0 },
    },
    {
        name: 'find a single route when stops allow it',
        options: { posA: 'A', posB: 'C', maxStops: 1 },
        edges: 'AB1, BC2',
        output: 1,
    },
    {
        name: 'does not find a route when none exists',
        options: { posA: 'A', posB: 'D', maxStops: 50 },
        edges: 'AB1, BC2',
        output: 0,
    },

    // Three routes configuration
    {
        name: 'finds multiple routes when enough stops are available',
        options: { posA: 'A', posB: 'D', maxStops: 3 },
        edges: 'AB1, BC2, AD5, CD3',
        output: 2,
    },
    {
        name: 'does not find a route when not enough stops are available',
        options: { posA: 'A', posB: 'D', maxStops: 1 },
        edges: 'AB1, BC2, CD3',
        output: 0,
    },
    {
        name: 'does not find a route when none exists',
        options: { posA: 'A', posB: 'F', maxStops: 50 },
        edges: 'AB1, BC2, CD3',
        output: 0,
    },

    // Four routes configuration
    {
        name: 'does not allow to revisit routes',
        options: { posA: 'A', posB: 'A', maxStops: 50 },
        edges: 'AB1, BA1, BC1, CA1',
        output: 1,
    },
    {
        name: 'finds many routes when stops allow',
        options: { posA: 'A', posB: 'D', maxStops: 50 },
        edges: 'AB1, AD5, BC2, BD5, CD3',
        output: 3,
    },
    {
        name: 'is not too greedy when finding routes with limited stops',
        options: { posA: 'A', posB: 'D', maxStops: 1 },
        edges: 'AB1, BC2, BD5, CD3, AD5',
        output: 2,
    },

    // Five routes configuration
    {
        name: 'finds many routes when distance allows',
        options: { posA: 'A', posB: 'D', maxStops: 50, maxDistance: 99 },
        edges: 'AB1, AD5, BC2, BD2, CD3',
        output: 3,
    },
    {
        name: 'is not too greedy when maximum distance is reached (1/2)',
        options: { posA: 'A', posB: 'D', maxStops: 50, maxDistance: 1 },
        edges: 'AB1, AD1, BC1, BD1, CD1',
        output: 2,
    },
    {
        name: 'is not too greedy when maximum distance is reached (2/2)',
        options: { posA: 'A', posB: 'D', maxStops: 50, maxDistance: 2 },
        edges: 'AB1, BD1, BC1, CD1, BF1, FG1, GD1, AD1',
        output: 3,
    },
    {
        name: 'finds no routes when distance is limited',
        options: { posA: 'A', posB: 'D', maxStops: 50, maxDistance: 0 },
        edges: 'AB1, BD1, BC1, CD1, BF1, FG1, GD1',
        output: 0,
    },
];

describe('routeExplorer', () => {
    describe('calculation of correct route data from sets', () => {
        routeConfigurations.forEach(({ options, edges, output, name }) => {
            const routeConfig = processData(edges);

            it(name, () => {
                const result = calculateRoutes(routeConfig, { ...options, name });

                expect(result.length).toEqual(output);
            });
        });
    });
});
