/**
 * Cheapest data processing
 */

import { extractEdges } from '../DeliveryCost';

describe('deliveryCost', () => {
    it('processes basic string data into something more useful', () => {
        const result = extractEdges('A-B-F');

        expect(result).toEqual(expect.arrayContaining(['AB', 'BF']));
    });

    it('Ignores invalid data parts', () => {
        const result = extractEdges('A-B-F-BB');

        expect(result).toEqual(expect.arrayContaining(['AB', 'BF']));
    });
});
