import React from 'react';
import PropTypes from 'prop-types';

import LabelledInput from 'components/LabelledInput';

import connectedData from './mapStateToProps';

import RouteCount from './components/RouteCount';
import { calculateRoutes } from './routeExplorer';

/**
 * Returns the cost of the cheapest route from options.posA, to options.posB.
 *
 * @param {Object[]} processedData - Processed data from redux.
 * @param {Object} options - Containing information about routing.
 */
function getCheapestRoute(processedData, options = {}) {
    const allRoutes = calculateRoutes(processedData, options);

    if (allRoutes.length < 1) {
        return 0;
    }

    const [cheapestRoute] = allRoutes.sort((a, b) => a.takenDistance - b.takenDistance);

    return cheapestRoute.takenDistance;
}

class DeliveryCheapest extends React.PureComponent {
    state = { cheapestRoute: 0 };

    componentDidMount() {
        this.onChange();
    }

    /**
     * Calculate the number of routes which are available and then re-render.
     */
    onChange = () => {
        const { processedData } = this.props;

        this.setState({
            cheapestRoute: getCheapestRoute(processedData, this.getDataArgs()),
        });
    };

    /**
     * Simply returns the current arguments from the components references.
     * @returns {Object{String: Number|String}} Arguments for the calculateRoutes functions.
     */
    getDataArgs() {
        const { posA, posB, maxDistance } = this;

        return {
            posA: posA.value,
            posB: posB.value,
        };
    }

    // Save references to our LabelledInput areas
    // allows stateless tracking of LabelledInput value
    stopRef = ref => {
        this.maxDistance = ref;
    };
    startRef = ref => {
        this.posA = ref;
    };
    endRef = ref => {
        this.posB = ref;
    };

    render() {
        const { cheapestRoute } = this.state;

        return [
            <h1 key="header">Available routes</h1>,
            <div className="row" key="route-input-area">
                <LabelledInput
                    type="text"
                    label="Start"
                    defaultValue="A"
                    setRef={this.startRef}
                    onChange={this.onChange}
                    placeholder="Start in town"
                />
                <LabelledInput
                    type="text"
                    label="Finish"
                    defaultValue="E"
                    setRef={this.endRef}
                    onChange={this.onChange}
                    placeholder="End in town"
                />
                <RouteCount label="Cheapest route">{cheapestRoute}</RouteCount>
            </div>,
        ];
    }
}

DeliveryCheapest.propTypes = {
    processedData: PropTypes.arrayOf(
        PropTypes.shape({
            edge: PropTypes.string.isRequired,
            distance: PropTypes.number.isRequired,
        })
    ),
};

export default connectedData(DeliveryCheapest);
