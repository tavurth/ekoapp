import { compose } from 'redux';
import DataDisplay from './DataDisplay';
import withConnect from './mapStateToProps';

export default compose(withConnect)(DataDisplay);
