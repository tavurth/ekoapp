import React from 'react';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import './style.scss';

import DeliveryCost from './DeliveryCost';
import DeliveryRoutes from './DeliveryRoutes';
import DeliveryCheapest from './DeliveryCheapest';

function DataDisplay() {
    return (
        <Tabs className="data-display">
            <TabList className="tab-header">
                <Tab>Case 1: Delivery Cost</Tab>
                <Tab>Case 2: Delivery Routes</Tab>
                <Tab>Case 3: Cheapest Route</Tab>
            </TabList>

            <TabPanel>
                <DeliveryCost />
            </TabPanel>

            <TabPanel>
                <DeliveryRoutes />
            </TabPanel>

            <TabPanel>
                <DeliveryCheapest />
            </TabPanel>
        </Tabs>
    );
}

export default DataDisplay;
