import Immutable from 'immutable';

/**
 * Takes in an array of already processed data, and returns a new Map.
 * The new map contains edge names, which map to another map, which contains the
 * distances from the primary key to the secondary key.
 *
 * @param {Edge[]} - Processed data from redux.
 * @returns {Map<String: Map<String: distance>>}
 * {
 *     townA: { townB: distanceAB }, { townC: distanceAC }
 *     townB: { townC: distanceBC }, { townD: distanceBD }
 * }
 */
export function arrayToEdgeMap(processedData) {
    const toReturn = new Map();

    processedData.forEach(({ townA, townB, distance }) => {
        // First time initializing this secondary layer
        // Set the primary key (townA) as an empty map
        if (!toReturn.has(townA)) {
            toReturn.set(townA, new Map());
        }

        // Get the current primary key
        // Set the distance to the secondary key inside of it
        toReturn.get(townA).set(townB, distance);
    });

    return toReturn;
}

/**
 * Takes in an object of options and applies defaults if not found.
 *
 * @param {Object} options - Options for the recursive search function.
 * @returns {Object} New object with defaults applied.
 */
export function addDefaultArguments(options) {
    return {
        // We'll use a map to keep track of how many times
        // we've visited each stop (if options.canRevisit is true)
        canRevisit: false,
        visited: Immutable.Map(),

        takenStops: 0,
        takenDistance: 0,
        maxStops: Infinity,
        maxDistance: Infinity,
        ...options,
    };
}

export function noRouteResult() {
    return null;
}

export function routeResult(lastStopBeforeFinal, { posB, visited, takenStops, takenDistance }) {
    return {
        takenStops,
        takenDistance,
        visited: visited.set(`${lastStopBeforeFinal}${posB}`, 1),
    };
}

/**
 * Set the routeName as visited in our routeMap.
 * If it's already set, we'll increase the number of visits.
 * @param {string} routeName - Parameter description.
 * @param {Map<String: Number>} visitedMap - Where string is the routeName,
 * and Number is the number of visits the route has seen { 'AB': 1 }.
 * @returns {Map<String: Number>}
 */
function setVisitedRoute(routeName, visitedMap) {
    // We've already seen this route, increment our counter
    if (visitedMap.has(routeName)) {
        return visitedMap.set(routeName, visitedMap.get(routeName) + 1);
    }

    // First time we've seen this route
    return visitedMap.set(routeName, 1);
}

/**
 * Returns true if we've already seen this route at least once.
 * @param {String} posA - Start of edge.
 * @param {String} posB - End of edge.
 * @param {String[]} visitedMap - All edges, example: ['AB', 'BC', 'BA'].
 */
function canUseRoute(posA, posB, { visited, canRevisit, name }) {
    if (visited.has(posA + posB)) {
        if (canRevisit === false) return false;

        // We've only allowed to use a certain route twice
        // as described in the bonus section of TASK.org
        if (visited.get(posA + posB) >= 2) return false;
    }

    if (visited.has(posB + posA)) {
        if (canRevisit === false) return false;

        // We've only allowed to use a certain route twice
        // as described in the bonus section of TASK.org
        if (visited.get(posB + posA) >= 2) return false;
        return true;
    }

    return true;
}

/**
 * Recursive helper function.
 * Checks a current position (posA) and then iterates forward over the edgesMap.
 * If we find the final position (posB) we'll return a success result
 * If we run out of gas, we'll return a failure result.
 * If we run out of stops, we'll return a failure result.
 *
 * @param {Map<String: Map<String: distance>>} edgesMap - Arguments for the calculateRoutes functions.
 * @param {Object} options - Which contains the general driver parameters
 * @param {String} options.posA - Current position of the driver in the map.
 * @param {String} options.posB - The final destination we're searching for.
 * @param {Number} options.maxStops - How many stops is the driver allowed to make?
 * @param {Number} options.maxDistance - How many km is the driver allowed to drive?
 * @param {Boolean} options.canRevisit - Are we allowed to revisit stops which we've already seen?
 * @param {Number} options.takenStops - How many stops has the driver already taken on this journey?
 * @param {Number} options.takenDistance - How far has the driver already driven on this journey?
 * @param {Map<String: Number>} options.visited - String is the edge, and Number is the count of visited times.
 * @returns {Array}
 */
function tryRouteRecursive(edgesMap, options) {
    const { posA: currentPosition } = options;

    if (!edgesMap.has(currentPosition)) {
        return noRouteResult(options);
    }

    // Handle the case when we're passed a maximum allowed distance
    const { takenDistance, maxDistance } = options;
    if (takenDistance > maxDistance) {
        return noRouteResult(options);
    }

    // Handle the case when we're passed a maximum number of stops
    const { takenStops, maxStops } = options;
    if (takenStops > maxStops) {
        return noRouteResult(options);
    }

    let toReturn = [];
    const currentEdge = edgesMap.get(currentPosition);

    // Check to see if the current route has the final position
    const { posB: endPosition } = options;
    if (currentEdge.has(endPosition)) {
        // Check that our current route is not the initial route
        // Fixes an issue where we find the end position in the second node
        // and return immediately, even though we're not allowed to do so
        if (canUseRoute(currentPosition, endPosition, options) === true) {
            toReturn = toReturn.concat(routeResult(currentPosition, options));
        }
    }

    const { canRevisit, visited } = options;

    // We've not found the current position, but we've still got gas to spare
    // We should continue looking through each of the towns from the current location
    // If at some point our gas runs out, we'll simple return [no result] from the recursion
    currentEdge.forEach((distance, townName) => {
        const edgeName = currentPosition + townName;

        // Check the visited list to see if we've already tried this route
        if (canUseRoute(currentPosition, townName, options) === false) {
            return;
        }

        // We'll recurse into ourselves here
        // We'll increment the distance and stops
        // If no limit is imposed these recursive calls will return a result of null
        const result = tryRouteRecursive(edgesMap, {
            ...options,
            posA: townName,

            // Keep track of how far we've travelled
            takenStops: takenStops + 1,
            takenDistance: takenDistance + distance,

            // Keep track of all the towns visited
            visited: setVisitedRoute(edgeName, visited),
        });

        toReturn = toReturn.concat(result);
    });

    // Return only non-null results
    return toReturn.filter(i => i);
}

/**
 * Calculates the number of available routse for a given cost.
 *
 * @param {Object} positionArgs - Start, end and maximum stops arguments.
 * @param {Object[]} processedData - Processed data points from redux state.
 */
export function calculateRoutes(processedData, positionArgs) {
    // Here we simply initialze our arguments parameter with some defaults
    positionArgs = addDefaultArguments(positionArgs);

    // Transform our array of processed data to a Map of Maps
    const edgesMap = arrayToEdgeMap(processedData);

    const { posA: startingPoint } = positionArgs;

    // Perhaps we don't even have that starting point
    // In that case, we have no route, just return 0
    if (!edgesMap.has(startingPoint)) {
        return [];
    }

    // Segue into the recursive helper function
    return (
        tryRouteRecursive(edgesMap, positionArgs)
            // We use an Immutable.Map to track visited
            // convert to JS here
            .map(({ visited, ...rest }) => ({
                ...rest,
                visited: visited.toJS(),
            }))
    );
}
