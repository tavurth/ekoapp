import React from 'react';
import PropTypes from 'prop-types';

import LabelledInput from 'components/LabelledInput';

import connectedData from './mapStateToProps';

import RouteCount from './components/RouteCount';
import { calculateRoutes } from './routeExplorer';

class DeliveryRoutes extends React.PureComponent {
    state = { nRoutes: 0 };

    componentDidMount() {
        this.onChange();
    }

    /**
     * Calculate the number of routes which are available and then re-render.
     */
    onChange = () => {
        const { processedData } = this.props;

        this.setState({
            nRoutes: calculateRoutes(processedData, this.getDataArgs()).length,
        });
    };

    /**
     * Simply returns the current arguments from the components references.
     * @returns {Object{String: Number|String}} Arguments for the calculateRoutes functions.
     */
    getDataArgs() {
        const { posA, posB, maxStops, canRevisit } = this;

        return {
            posA: posA.value,
            posB: posB.value,
            canRevisit: canRevisit.checked,
            maxStops: Number.parseInt(maxStops.value, 10),
        };
    }

    // Save references to our LabelledInput areas
    // allows stateless tracking of LabelledInput value
    stopRef = ref => {
        this.maxStops = ref;
    };
    startRef = ref => {
        this.posA = ref;
    };
    endRef = ref => {
        this.posB = ref;
    };
    revisitRef = ref => {
        this.canRevisit = ref;
    };

    render() {
        const { nRoutes } = this.state;

        return [
            <h1 key="header">Available routes</h1>,
            <div className="row" key="input-result-area">
                <LabelledInput
                    label="Can revisit?"
                    type="checkbox"
                    defaultValue={false}
                    setRef={this.revisitRef}
                    onChange={this.onChange}
                />
                <LabelledInput
                    type="text"
                    label="Start"
                    defaultValue="A"
                    setRef={this.startRef}
                    onChange={this.onChange}
                    placeholder="Start in town"
                />
                <LabelledInput
                    type="text"
                    label="Finish"
                    defaultValue="E"
                    setRef={this.endRef}
                    onChange={this.onChange}
                    placeholder="End in town"
                />
                <LabelledInput
                    type="text"
                    label="No. Stops"
                    defaultValue="4"
                    setRef={this.stopRef}
                    placeholder="No. Stops"
                    onChange={this.onChange}
                />
                <RouteCount label="Routes found">{nRoutes}</RouteCount>
            </div>,
        ];
    }
}

DeliveryRoutes.propTypes = {
    processedData: PropTypes.arrayOf(
        PropTypes.shape({
            edge: PropTypes.string.isRequired,
            distance: PropTypes.number.isRequired,
        })
    ),
};

export default connectedData(DeliveryRoutes);
