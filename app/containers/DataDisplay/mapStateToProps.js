import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectRawData, makeSelectProcessedData } from './selectors';

export const mapStateToProps = createStructuredSelector({
    rawData: makeSelectRawData(),
    processedData: makeSelectProcessedData(),
});

export default connect(mapStateToProps);
