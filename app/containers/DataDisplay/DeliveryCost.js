import React from 'react';
import PropTypes from 'prop-types';
import LabelledInput from 'components/LabelledInput';

import connectedData from './mapStateToProps';
import RouteCount from './components/RouteCount';

/**
 * Extracts the edges from the string.
 * Where string is A-E-F, returns ['AE', 'EF']
 * @param {String} string - Raw string to extract edges from.
 * @returns {String[]} Edges
 */
export function extractEdges(string) {
    const toReturn = [];
    let currentPart = '';
    string
        .split('-')
        .filter(i => i.length === 1)
        .forEach(part => {
            part = part.toUpperCase();

            if (currentPart.length === 0) {
                currentPart += part;
                return;
            }

            toReturn.push(currentPart + part);

            // Setup for the next cycle
            currentPart = part;
        });

    // Add any last parts
    if (currentPart.length === 2) {
        toReturn.push(currentPart);
    }

    return toReturn;
}

/**
 * Calculates the simple cost of a string A-E-F.
 * @param {String} string - String to be parsed and cost calculations performed.
 * @param {Object[]} processedData - Processed edge and distance data from redux.
 * @returns {Number} Cost of journey.
 */
function calculateCost(string, processedData) {
    const edges = extractEdges(string);

    const foundEdges = edges.map(
        edge =>
            processedData.filter(
                ({ edge: dataEdge }) =>
                    // Filter the processed data to find each edge in our edges path
                    // If the edge from the processed data set 'AB' === 'AB' (from our path)
                    // we'll return the edge from the processed data
                    dataEdge === edge
            )[0]
    );

    // If any of the edges sought does not exist
    // there is no path to the distantion
    if (!foundEdges.every(i => i)) {
        return 0;
    }

    // Simply sum the edge distances
    return foundEdges.reduce((acc, { distance }) => acc + distance, 0);
}

class DeliveryCost extends React.PureComponent {
    state = { cost: 0 };

    onChange = () => {
        const { costInput } = this;
        const { processedData } = this.props;

        this.setState({ cost: calculateCost(costInput.value, processedData) });
    };

    costRef = ref => {
        this.costInput = ref;
    };

    render() {
        const { cost } = this.state;

        return [
            <h1 key="header">Delivery Cost</h1>,
            <div className="row" key="cost-input-and-display">
                <div className="cost-string-input">
                    <LabelledInput
                        type="text"
                        label="Route input"
                        defaultValue="A-B-F"
                        id="costCalulation"
                        setRef={this.costRef}
                        onChange={this.onChange}
                        placeholder="Enter town data here"
                    />
                </div>
                <RouteCount label="Final cost">{cost}</RouteCount>
            </div>,
            // prettier-no-wrap
        ];
    }
}

DeliveryCost.propTypes = {
    processedData: PropTypes.arrayOf(
        PropTypes.shape({
            edge: PropTypes.string.isRequired,
            distance: PropTypes.number.isRequired,
        })
    ),
};
export default connectedData(DeliveryCost);
