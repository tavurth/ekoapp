import React from 'react';
import { Helmet } from 'react-helmet';

import { stepNext } from 'containers/HomePage/actions';
import { dataSave } from './actions';
import './style.scss';

function pushDataToRedux(data) {
    stepNext();
    dataSave(data);
}

const DEFAULT_COSTS =
    'AB1, AB3, BA3, AC4, AD10, BE3, CD4, CF2, DE1, EB3, FD1, EA14, BC8, CD5, EF23, FG12, GH8, HI1, IJ3, JK7, KL19, LM22, MN2, NO23, OP14, PQ18, QR28, RS16, ST32, TU23, UV10, VW11, WX13, XY16, YZ42';

class DataInput extends React.PureComponent {
    townRef = ref => {
        this.townRef = ref;
    };

    saveData = event => {
        event.preventDefault();

        const { townRef } = this;
        pushDataToRedux(townRef.value);
    };

    render() {
        return (
            <article>
                <Helmet>
                    <title>EkoApp::TSP</title>
                    <meta name="description" content="EkoApp -> Travelling Salesman Problem" />
                </Helmet>
                <div className="town-input">
                    <section>
                        <form onSubmit={this.saveData}>
                            <label htmlFor="town-data">
                                <textarea
                                    type="text"
                                    id="townData"
                                    ref={this.townRef}
                                    defaultValue={DEFAULT_COSTS}
                                    placeholder="Enter town distance data here"
                                />
                            </label>
                            <div className="row">
                                <button type="submit">Submit</button>
                            </div>
                        </form>
                    </section>
                </div>
            </article>
        );
    }
}

export default DataInput;
