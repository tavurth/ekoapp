/**
 * Test data processing
 */

import { processData } from '../actions';

describe('dataProcessing', () => {
    it('processes basic data into something more useful', () => {
        const result = processData('AB1, BC2, CD3');

        expect(result).toEqual(
            expect.arrayContaining([
                {
                    townA: 'A', townB: 'B', edge: 'AB', distance: 1
                },
                {
                    townA: 'B', townB: 'C', edge: 'BC', distance: 2
                },
                {
                    townA: 'C', townB: 'D', edge: 'CD', distance: 3
                },
            ])
        );
    });

    it('does not process badly formatted data', () => {
        const result = processData('AB, BC2, CD3');

        expect(result).toEqual(
            expect.arrayContaining([
                // prettier-no-wrap
                {
                    townA: 'B', townB: 'C', edge: 'BC', distance: 2
                },
                {
                    townA: 'C', townB: 'D', edge: 'CD', distance: 3
                },
            ])
        );
    });

    it('processes more than one number', () => {
        const result = processData('AB, AD10, CD3');

        expect(result).toEqual(
            expect.arrayContaining([
                // prettier-no-wrap
                {
                    townA: 'A', townB: 'D', edge: 'AD', distance: 10
                },
                {
                    townA: 'C', townB: 'D', edge: 'CD', distance: 3
                },
            ])
        );
    });
});
