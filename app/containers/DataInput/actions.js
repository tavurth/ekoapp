import { dispatch } from 'getStore';
import { DATA_SAVE, DATA_RESET } from './constants';

const DATA_MATCH_REGEXP = /(..)(\d+)/;

/**
 * Filters data after transformation.
 * @param {array} transformedData - Array of data items extracted from raw string.
 * @returns {boolean} True if the data is correctly formatted.
 */
function dataFilter(transformedData) {
    if (!transformedData) return false;

    // eslint-disable-next-line no-unused-vars
    const [_, edge] = transformedData;

    if (typeof edge !== 'string') return false;
    if (edge.length < 2) return false;

    return true;
}

/**
 * Process the raw string data from town distances to something more useful.
 *
 * AB1, BC2, CD3 => [{edge:'AB', distance: 1}, {edge:'BC', distance: 2}, {edge:'CD', distance: 3}]
 * @param {string} data - String input from the DataInput textarea.
 */
export function processData(data) {
    const splitData = data.toUpperCase().split(',');
    if (splitData.length < 1) {
        return [];
    }

    return (
        splitData

            // First we'll trim the split data, and extract the parts
            // according to the regexp above
            .map((dataItem) => dataItem.trim().match(DATA_MATCH_REGEXP))

            // Filter out bad values
            .filter(dataFilter)

            // Then just return a bunch of objects
            // eslint-disable-next-line no-unused-vars
            .map(([_, edge, distance]) => ({
                edge,
                townA: edge[0],
                townB: edge[1],
                distance: Number.parseInt(distance, 10),
            }))

            // Handles a specific case (Do not count zero cost delivery routes)
            .filter(({ distance }) => distance >= 0)
    );
}

export function dataSave(data) {
    return dispatch({
        type: DATA_SAVE,
        payload: {
            raw: data,
            processed: processData(data),
        },
    });
}

export function dataReset() {
    return dispatch({
        type: DATA_RESET,
    });
}
