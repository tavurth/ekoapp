import { fromJS } from 'immutable';

import { DATA_SAVE, DATA_RESET } from './constants';

// The initial state of the App
const initialState = fromJS({
    rawData: null,
    loading: false,
});

function appReducer(state = initialState, action) {
    switch (action.type) {
        case DATA_SAVE: {
            const { processed, raw } = action.payload;
            return state.set('rawData', raw).set('processedData', processed);
        }
        case DATA_RESET:
            return state.set('rawData', null);
        default:
            return state;
    }
}

export default appReducer;
